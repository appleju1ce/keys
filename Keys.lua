Keys = LibStub("AceAddon-3.0"):NewAddon("Keys", "AceConsole-3.0", "AceEvent-3.0", "AceComm-3.0")
local AceGUI = LibStub("AceGUI-3.0")
local addon = Keys
local version
local addonName = ...

local keyStoneItemID = 158923
-- debugging aktuell HS ID LG
--local keyStoneItemID = 6948

local charname
local realm
local rname
local class
local faction
local friends = {}

local currentTime
local currentRegion
local questReset
local weekday
local lastUpdate

local frame
local guiOpened = false

local defaults = {
	global = {
		key = {},
	}
}

local classColor = {
	[1] = "C79C6E", -- Warrior
	[2] = "F58CBA", -- Paladin
	[3] = "ABD473", -- Hunter
	[4] = "FFF569", -- Rogue
	[5] = "FFFFFF", -- Priest
	[6] = "C41F3B", -- DeathKnight
	[7] = "0070DE", -- Shaman
	[8] = "69CCF0", -- Mage
	[9] = "9482C9", -- Warlock
	[10] = "00FF96", -- Monk
	[11] = "FF7D0A", -- Druid
	[12] = "A330C9" -- Demon Hunter
}

-- Legion Dungeons -- TODO BfA Dungeons 
local instances = {
	[1456] = 197, -- EoA
	[1466] = 198, -- DHT
	[1501] = 199, -- BRH
	[1477] = 200, -- HoV
	[1458] = 206, -- NL
	[1493] = 207, -- VotW
	[1492] = 208, -- MoS
	[1516] = 209, -- Arcway
	[1571] = 210, -- CoS
	[1651] = 227, -- Lower Kara
	[1677] = 233, -- CoEN
	[1651] = 234, -- Upper Kara
	[1753] = 239, -- Seat of the Triumvirate
}

function addon:OnInitialize()
	self.db = LibStub("AceDB-3.0"):New("KeysDB", defaults, true);
	self.db.global.key = self.db.global.key or {}
	self.db.global.guild = self.db.global.guild or {}
	self.db.global.dropdown = self.db.global.dropdown or 1
	self:RegisterChatCommand("keys", "ChatCommand")
end

function addon:OnEnable()
	version = GetAddOnMetadata(addonName, "version")
	charname = UnitName("player")
	realm = GetRealmName()
	rname = charname.."-"..realm
	class = select(3, UnitClass("player"))
	faction = UnitFactionGroup("player")
	currentWeekday = C_Calendar.GetDate().weekday
	questReset = GetQuestResetTime()
	currentTime = GetServerTime()
	lastUpdate = 0
	currentRegion = GetCurrentRegion()
	self.db.global.reset = self:idReset(currentRegion, currentWeekday, questReset, currentTime)
	
	self:RegisterComm("OMEGAKEYS")
	self:RegisterEvent("PLAYER_ENTERING_WORLD")
	self:RegisterEvent("BAG_UPDATE")
	self:RegisterEvent("CHALLENGE_MODE_KEYSTONE_RECEPTABLE_OPEN")
end

function addon:PLAYER_ENTERING_WORLD()
	self:scanBags()	
	self:sendKeys()
end

function addon:BAG_UPDATE()
	self:scanBags()
	self:sendKeys()
end

function addon:sendKeys()
	if (lastUpdate + 60) < currentTime then
		lastUpdate = currentTime
		if friends == nil or friends ~= self:getFriends() then 
			friends = nil
			friends = self:getFriends()
			for k, v in pairs(self.db.global.key) do
				self:SendCommMessage("OMEGAKEYS", k..","..v.link..","..v.class..","..v.timestamp..","..rname, "GUILD")
				for key, value in pairs(friends) do
					self:SendCommMessage("OMEGAKEYS", k..","..v.link..","..v.class..","..v.timestamp..","..rname, "WHISPER", value)
				end
			end
		end
	end
end
--/dump Keys:getFriends()
function addon:getFriends()
	local friends = {}
	for i = 1, select(2, BNGetNumFriends()) do
		local bnetIDGameAccount = select(6,BNGetFriendInfo(i))
		local _, characterName, _, realmName, _, fac = BNGetGameAccountInfo(bnetIDGameAccount)	
		if characterName ~= nil and characterName ~= "" and realmName ~= nil and realmName ~= "" and fac == faction then
				table.insert(friends, characterName.."-"..realmName)
		end
	end
	return friends
end

function addon:CHALLENGE_MODE_KEYSTONE_RECEPTABLE_OPEN()
	local instanceId = select(8, GetInstanceInfo())
	if (self.db.global.key[rname] and (select(3, self:getKeyinformation(self.db.global.key[rname].link)) == instances[instanceId])) then
		self:insertKey()
	end
end

function addon:OnCommReceived(prefix, message, distribution, sender)
	local msg = {strsplit(",", message)} -- 1 name, 2 Keylink, 3 class, 4 Timestamp, 5 realm + charname
	local name = msg[1]
	local link = msg[2]
	local class = msg[3]
	local timestamp = msg[4]
	local rname = msg[5]

	if(self.db.global.key ~= nil and self.db.global.key[name]) then return end -- if own char
	self.db.global.guild[name] = self.db.global.guild[name] or {}
	if ((self.db.global.guild[name].timestamp == nil) or (rname == name) or (self.db.global.guild[name].timestamp < tonumber(timestamp))) then
		self.db.global.guild[name].link = link
		self.db.global.guild[name].class = tonumber(class)
		self.db.global.guild[name].timestamp = tonumber(timestamp)
	end
end

function addon:setKey(link)
	self.db.global.key[rname] = self.db.global.key[rname] or {}
	self.db.global.key[rname].link = link
	self.db.global.key[rname].class = class
	self.db.global.key[rname].timestamp = tonumber(currentTime)
	self.db.global.key[rname].hide = self.db.global.key[rname].hide or false
end

function addon:remAllKeys()
	for k in pairs(self.db.global.key) do
		self.db.global.key [k] = nil
	end	
	for k in pairs(self.db.global.guild) do
		self.db.global.guild [k] = nil
	end	
end

function addon:ChatCommand(input)
	input = strlower(input)
	if (#input == 0) then
		self:showUI()
	elseif(input == "lul") then
		--TODO
	end
end

function addon:drawTab(container, data)
	if (data == nil) then return end
	local labels = {}
	local scroll = AceGUI:Create("ScrollFrame")
	scroll:SetFullWidth(true)
	scroll:SetFullHeight(true)
	scroll:SetLayout("Flow")
	container:AddChild(scroll)
	
	-- sorting
	local sortedTbl = {}
	for k, v in pairs(data) do
		v.name = k
		table.insert(sortedTbl, v)
	end
	table.sort(sortedTbl, function(a, b) return select(2,self:getKeyinformation(a.link)) > select(2,self:getKeyinformation(b.link)) end)
	for k, v  in pairs(sortedTbl) do			
			if(not v.hide) then
			labels[k.."_label"] = AceGUI:Create("InteractiveLabel")
			labels[k.."_label"]:SetImage(GetItemIcon(keyStoneItemID))
			labels[k.."_label"]:SetImageSize(15,15)
			labels[k.."_label"]:SetRelativeWidth(0.8)
			labels[k.."_label"]:SetText("|cFF".. classColor[v.class] .. v.name .. "|r: " .. select(1,self:getKeyinformation(v.link).. " ".. select(2, self:getKeyinformation(v.link))))
			labels[k.."_label"]:SetCallback("OnEnter", function(widget) 
				GameTooltip_SetDefaultAnchor( GameTooltip, UIParent )
				GameTooltip:SetHyperlink(v.link)
				GameTooltip:Show()
			end)
			labels[k.."_label"]:SetCallback("OnLeave", function(widget) 
					GameTooltip:Hide()
			end)
			labels[k.."_label"]:SetCallback("OnClick", function(widget) 
				if (IsModifiedClick()) then
					HandleModifiedItemClick(v.link)
				end
			end)
			scroll:AddChild(labels[k.."_label"])
		end	
	end
	
end

function addon:showUI()
	if (guiOpened) then
		frame:Release()
		guiOpened = false
		return
	end
	frame = AceGUI:Create("Frame")
	frame:SetTitle("Keystone Manager")
	frame:SetStatusText("Version: " .. version)
	frame:SetCallback("OnClose", function(widget) 
		AceGUI:Release(widget)
		guiOpened = false
	end)
	frame:SetWidth(400)
	frame:SetHeight(250)
	frame:EnableResize(false)
	frame:SetLayout("Flow")

	-- dropdown menu
	self.db.global.dropdown = self.db.global.dropdown or 0
	local menu = {
		keys = "Alts", 
		friends = "Friends & Guild",
	}
	local order = {"keys" , "friends"}
	local dropdown = AceGUI:Create("DropdownGroup")
	dropdown:SetFullWidth(true)
	dropdown:SetFullHeight(true)
	dropdown:SetTitle("select")
	dropdown:SetGroupList(menu, order)
	dropdown:SetCallback("OnGroupSelected", function(widget, event, group)
		widget:ReleaseChildren()
		if(group == "keys") then
			self.db.global.dropdown = 1
			self:drawTab(widget, self.db.global.key)
		elseif group == "friends" then
			self.db.global.dropdown = 2
			self:drawTab(widget, self.db.global.guild)
		end	
	end)
	dropdown:SetGroup(order[self.db.global.dropdown])
	dropdown:DoLayout()
	
	frame:AddChild(dropdown)
	guiOpened = true
end

function addon:getKeyinformation(key)
	if (key == nil) then
		return nil
	end
	-- 1 shit, 2 keyId, 3 mapId, 4 keyLevel
	local KeyInfo = {strsplit(":", key)}
	local mapName = C_ChallengeMode.GetMapUIInfo(tonumber(KeyInfo[3])) 
	local level = tonumber(KeyInfo[4])
	
	return mapName, level
end

function addon:scanBags()
	for i = 0, NUM_BAG_SLOTS do
		local maxSlots = GetContainerNumSlots(i)
		for j = 1, maxSlots do		
			if (GetContainerItemID(i, j) == keyStoneItemID) then
				local link = GetContainerItemLink(i, j)
				self:setKey(link)
			end			
		end
	end
end

function addon:insertKey() 
	for i = 0, NUM_BAG_SLOTS do
		local maxSlots = GetContainerNumSlots(i)
		for j = 1, maxSlots do		
			if (GetContainerItemID(i, j) == keyStoneItemID) then
				PickupContainerItem(i, j)
				if(CursorHasItem()) then
					C_ChallengeMode.SlotKeystone()
				end
			end			
		end
	end
end

--[[
1 sunday 
2 monday
3 tuesday
4 wednesday
5 thursday
6 friday
7 saturday
--]]
function addon:idReset(region, weekday, reset, curTime)
	local ret = self.db.global.reset or 0

	if ((ret == nil) or (ret < curTime)) then
		local numbers = {}
		numbers[1] = {1, 0, 6 , 5, 4 , 3, 2} -- us
		numbers[2] = {2, 1, 0 , 6, 5 , 4, 3} -- korea TODO 
		numbers[3] = {2, 1, 0 , 6, 5 , 4, 3} -- europe
		numbers[4] = {2, 1, 0 , 6, 5 , 4, 3} -- taiwan TODO
		numbers[5] = {2, 1, 0 , 6, 5 , 4, 3} -- china TODO

		self:remAllKeys()
		self:scanBags()		
		ret = curTime + (86400 * numbers[region][weekday]) + reset
	end
	return ret
end